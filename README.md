# ansible-jenkins-build

This Ansible playbook installs the Jenkins CI server & also has the provision to install optional Jenkins plugins.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

The following prerequisites are required to invoke this code:

* Running a Linux/Unix host
* Vagrant
* Ansible
* Serverspec

### Installing/provisioning

Clone this repository to a directory on your local machine:

```
git@gitlab.com:PLAR/ansible-jenkins-build.git
```
### Optional parameters

This playbook supports the optional parameters:

* Firewalld configuration (defaults stored in role/defaults/main.yaml)

```
jenkins_firewall_port_list:
  - { port: "8080", proto: "tcp" }
```

* Installation of Jenkins plugins (default example stored in role/defaults/main.yaml)

```
jenkins_plugin_list:
  - greenballs
```
Jenkins plugin names can be acquired from [Here](https://plugins.jenkins.io/).

### Notes

The web port to access Jenkins has been redirected to 8081 (because the default was in use on my device).

### Building VM using Vagrant & accessing service.

To spin up a VM, simply do the following:

```
cd ansible-jenkins-build
vagrant up
```

Once the Ansible playbook has been completed, in the output you will see an item similar to this:

```
TASK [Jenkins : Acquiring Jenkins admin password]

ok: [Jenkins] => {
    "msg": "Jenkins initial admin password: 5b44e21813b6423bb76a4c9ae5f78f4e"
}
```
This is your Jenkins initial admin password, you will need this when you first login to Jenkins.
By default Jenkins will be available [Here](http://localhost:8081)


## Built With

* [Vagrant](https://www.vagrantup.com/) - VM provisioning platform
* [Ansible](https://www.ansible.com/) - Configuration management platform
* [Serverspec](https://serverspec.org/) - Validation platform

## Future Enhancements

* Make Ansible generate Serverspec file dynamically based on playbook options.

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/PLAR/ansible-jenkins-build/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.


## Authors

* **David McElin** - *Initial work* - [Fast-Trak Solutions Ltd](https://ftsol.co.uk)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Plugin Installation script based on work by Michael Wyraz - https://github.com/micw
