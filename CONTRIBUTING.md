# Contributing

We are open to, and grateful for, any contributions made by the community. By contributing to ansible-jenkins-build, you agree to abide by the [code of conduct](https://gitlab.com/PLAR/ansible-jenkins-build/blob/master/CODE_OF_CONDUCT.md).

## Reporting Issues and Asking Questions

Before opening an issue, please search the [issue tracker](https://gitlab.com/PLAR/ansible-jenkins-build/issues) to make sure your issue hasn't already been reported.

### Bugs and Improvements

We use the issue tracker to keep track of bugs and improvements to ansible-jenkins-build itself, its examples, and the documentation. We encourage you to open issues to discuss improvements, architecture, theory, internal implementation, etc. If a topic has been discussed before, we will ask you to join the previous discussion.
