describe package('epel-release') do
  it { should be_installed }
end

describe package('git') do
  it { should be_installed }
end

describe package('unzip') do
  it { should be_installed }
end

describe package('java-1.8.0-openjdk') do
  it { should be_installed }
end

describe package('jenkins') do
  it { should be_installed }
end

describe service('jenkins') do
  it { should be_running }
  it { should be_enabled }
end

describe port(8080) do
  it { should be_listening }
end
